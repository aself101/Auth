/*
  Loops through all actions until the one required is reached

*/
export default function({ dispatch }) {
  return (next) => (action) => {
    // If the action does not have a payload or is not a promise; send on to next set of actions
    if (!action.payload || !action.payload.then) {
      return next(action);
    }
    // Make sure action's promise resolves
    action.payload
      .then((response) => {
          const newAction = { ...action, payload: response };
          dispatch(newAction);
       });
  }
}
