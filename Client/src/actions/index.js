import axios from 'axios';
import { browserHistory } from 'react-router';

import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, FETCH_MESSAGE } from './types';

const ROOT_URL = 'http://localhost:3090';

export function signinUser({email, password}) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signin`, { email, password })
      .then((response) => {
        // Save JSON Web Token to localStorage
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('email', email);
        // Authenticate user on redux front end
        dispatch({ type: AUTH_USER });
        // Send user to protected component
        browserHistory.push('/feature');
      })
      .catch(() => {
        dispatch(authError('Bad login Info'));
      });
  };
}

export function signupUser({email, password}) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signup`, { email, password })
      .then((response) => {
        localStorage.setItem('token', response.data.token);
        dispatch({ type: AUTH_USER });
        browserHistory.push('/feature');
      })
      .catch(() => {
        dispatch(authError('Email is already in use'));
      });
  }
}

export function authError(err) {
  return {
    type: AUTH_ERROR,
    payload: err
  };
}

export function signoutUser() {
  // Destroy JWT token
  localStorage.removeItem('token');
  localStorage.removeItem('email');
  return {
    type: UNAUTH_USER
  }
}

export function fetchMessage() {
  return function(dispatch) {
    axios.get(ROOT_URL, {
      headers: { authorization: localStorage.getItem('token') }
    }).then((response) => {
      dispatch({type: FETCH_MESSAGE, payload: response.data.message});
    });
  }
}











/* END */
